package com.zessta.myapplication.screen;

import android.os.Bundle;
import android.util.Log;

import com.zessta.myapplication.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class ActivityLifeCycle extends AppCompatActivity {

    public static final String TAG = ActivityLifeCycle.class.getName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "On Create is Called");
        setContentView(R.layout.activity_life_cycle);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "On Start is called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "On Resume is called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "On Pause is called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "On Stop is called");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "On Restart is called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "On Destroy is called");
    }
}
