package com.zessta.myapplication.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.zessta.myapplication.screen.ServiceActivity;

import java.util.Random;

import androidx.annotation.Nullable;

public class ServiceActivityService extends Service {

    public static final String TAG = ServiceActivityService.class.getName();

    private IBinder myBinder = new MyBinder();

    public int randomInteger;
    private Thread thread;

    @Override
    public void onCreate() {
        Log.d(TAG, " OnCreate");
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, " OnStart Command" + intent.getExtras().getString("Name"));
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind: ");
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Random random = new Random();
                    randomInteger = random.nextInt();
                    Log.d(TAG, "run: " + randomInteger);
                }
            }
        });
        thread.start();
        return myBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "onUnbind: ");
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        Log.d(TAG, "onRebind: ");
        super.onRebind(intent);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, " on destroy");
        thread.stop();
        super.onDestroy();
    }

    public class MyBinder extends Binder {
        public ServiceActivityService getService() {
            return ServiceActivityService.this;
        }
    }
}
