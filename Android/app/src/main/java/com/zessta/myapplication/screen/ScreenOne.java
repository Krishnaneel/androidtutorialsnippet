package com.zessta.myapplication.screen;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.zessta.myapplication.MainActivity;
import com.zessta.myapplication.R;

import androidx.appcompat.app.AppCompatActivity;

public class ScreenOne extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_one);
        Button button = findViewById(R.id.send_to_main_activity_btn);
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        };
        button.setOnClickListener(onClickListener);
        Bundle getBundle = getIntent().getExtras();
        if(getBundle != null) {
            String name = getBundle.getString("Name");
            boolean human = getBundle.getBoolean("human");
            TextView sampleTextView = findViewById(R.id.sampleValuesTextView);
            sampleTextView.setText("Name : " + name + " human " + human);
        }
    }
}
