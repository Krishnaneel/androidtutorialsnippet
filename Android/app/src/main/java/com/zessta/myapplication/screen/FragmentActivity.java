package com.zessta.myapplication.screen;

import android.os.Bundle;

import com.zessta.myapplication.Fragments.FragmentTitle;
import com.zessta.myapplication.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class FragmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        FragmentTitle fragmentOne = new FragmentTitle("Fragment One");
        FragmentTitle fragmentTwo = new FragmentTitle("Fragment Two");
        FragmentTitle fragmentThree = new FragmentTitle("Fragment Three");

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_one_layout, fragmentOne).commit();
        fragmentTransaction.add(R.id.fragment_two_layout, fragmentTwo);
        fragmentTransaction.add(R.id.fragment_three_layout, fragmentThree).commit();
    }
}
