package com.zessta.myapplication;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.zessta.myapplication.assignment.FragmentAddAcitivity;
import com.zessta.myapplication.assignment.SecondActivity;
import com.zessta.myapplication.screen.ActivityLifeCycle;
import com.zessta.myapplication.screen.FragmentActivity;
import com.zessta.myapplication.screen.ScreenOne;
import com.zessta.myapplication.screen.ServiceActivity;

public class MainActivity extends AppCompatActivity {

    public static String android = "android";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppCompatButton moveToScreenOne = findViewById(R.id.toScreenOneBtn);
        moveToScreenOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Creating an intent to move to screenOne.
                Intent intent = new Intent(MainActivity.this, ScreenOne.class);
                Bundle bundle = new Bundle();
                bundle.putString("Name", "Krishna");
                bundle.putBoolean("human", true);
                intent.putExtras(bundle);
                startActivity(intent);
                // To kill the activity and it doesnt add to the backstack.
                finish();
            }
        });

        AppCompatButton moveToActivityLifeCycle = findViewById(R.id.toActivityLifeCycleBtn);
        moveToActivityLifeCycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Creating an intent to move to ActivityLifeCycle
                Intent intent = new Intent(MainActivity.this, ActivityLifeCycle.class);
                startActivity(intent);
            }
        });

        AppCompatButton moveToFragmentActivity = findViewById(R.id.toFragmentActivityBtn);
        moveToFragmentActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Creating an intent to move to ActivityLifeCycle
                Intent intent = new Intent(MainActivity.this, FragmentActivity.class);
                startActivity(intent);
            }
        });

        AppCompatButton moveToSecondActivity = findViewById(R.id.toAssignmentSecondActivity);
        moveToSecondActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                Bundle bundle = new Bundle();
                bundle.putBoolean("isBoring", true);
                bundle.putString("Assignment"," hell yeah");
                intent.putExtras(bundle);
                startActivityForResult(intent, 20);
            }
        });

        AppCompatButton moveToSecondFragmentActivity = findViewById(R.id.toAssignmentSecondFragmentActivity);
        moveToSecondFragmentActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Creating an intent to move to ActivityLifeCycle
                Intent intent = new Intent(MainActivity.this, FragmentAddAcitivity.class);
                startActivity(intent);
            }
        });

        AppCompatButton moveToServiceActivity = findViewById(R.id.toServiceActivity);
        moveToServiceActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Creating an intent to move to ActivityLifeCycle
                Intent intent = new Intent(MainActivity.this, ServiceActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(requestCode == 20 ) {
            if(resultCode == RESULT_OK) {
                // This is used to show the Toast.
                Toast.makeText(MainActivity.this, intent.getExtras().getString("Name"), Toast.LENGTH_LONG).show();
            }
        }
    }
}
