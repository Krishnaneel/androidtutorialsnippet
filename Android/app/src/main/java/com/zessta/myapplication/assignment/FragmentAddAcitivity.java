package com.zessta.myapplication.assignment;

import android.os.Bundle;
import android.view.View;

import com.zessta.myapplication.Fragments.FragmentTitle;
import com.zessta.myapplication.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class FragmentAddAcitivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_add_activity);

        findViewById(R.id.add_fragment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTitle fragmentTitle = new FragmentTitle("This is assignment");
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.assignment_fragment_container, fragmentTitle).addToBackStack(null).commit();
                fragmentTransaction.add(R.id.assignment_fragment_container, fragmentTitle).addToBackStack(null).commit();

//                fragmentManager.popBackStack();
            }
        });
    }
}
