package com.zessta.myapplication.screen;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.Toast;

import com.zessta.myapplication.R;
import com.zessta.myapplication.service.ServiceActivityService;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

public class ServiceActivity extends AppCompatActivity implements View.OnClickListener {

    private ServiceActivityService serviceActivityService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.service_class_activity);

        AppCompatButton startService = findViewById(R.id.start_service_btn);
        AppCompatButton getValue = findViewById(R.id.get_value_btn);
        AppCompatButton stopService = findViewById(R.id.stop_service_btn);

        startService.setOnClickListener(this);
        getValue.setOnClickListener(this);
        stopService.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.start_service_btn: {
                Intent intent = new Intent(ServiceActivity.this, ServiceActivityService.class);
                Bundle bundle = new Bundle();
                bundle.putString("Name", "Krishna");
                intent.putExtras(bundle);
                startService(intent);
                bindService(intent, serviceConnection, Context.BIND_NOT_FOREGROUND);
                break;
            }
            case R.id.get_value_btn: {
                Toast.makeText(ServiceActivity.this, serviceActivityService.randomInteger + "", Toast.LENGTH_LONG).show();
                break;
            }

            case R.id.stop_service_btn: {
                Intent intent = new Intent(ServiceActivity.this, ServiceActivityService.class);
//                unbindService(serviceConnection);
                stopService(intent);
                break;
            }
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            ServiceActivityService.MyBinder binder = (ServiceActivityService.MyBinder) iBinder;
            serviceActivityService = binder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            serviceActivityService = null;
        }
    };
}
