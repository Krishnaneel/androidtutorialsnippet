package com.zessta.expenseplanner.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.zessta.expenseplanner.R;
import com.zessta.expenseplanner.modal.Expense;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ExpenseListAdapter extends RecyclerView.Adapter {
    ArrayList<Expense> expenses;

    public ExpenseListAdapter(ArrayList<Expense> expenses) {
        this.expenses = expenses;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View expenseView = inflater.inflate(R.layout.expense_list_item, parent, false);
        return new ExpenseViewHolder(expenseView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ExpenseViewHolder viewHolder = (ExpenseViewHolder) holder;
        viewHolder.setValue(expenses.get(position));
    }

    @Override
    public int getItemCount() {
        return expenses.size();
    }

    public class ExpenseViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView amount;
        TextView date;


        public ExpenseViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.expense_title);
            amount = itemView.findViewById(R.id.expense_amount);
            date = itemView.findViewById(R.id.expense_date);
        }

        public void setValue(Expense expense) {
            title.setText(expense.title);
            amount.setText("₹"+ expense.amount);
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            date.setText(formatter.format(expense.date));
        }
    }
}
