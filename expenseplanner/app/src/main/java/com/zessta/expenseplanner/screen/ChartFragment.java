package com.zessta.expenseplanner.screen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.chart.common.listener.Event;
import com.anychart.chart.common.listener.ListenersInterface;
import com.anychart.charts.Pie;
import com.anychart.enums.Align;
import com.anychart.enums.LegendLayout;
import com.zessta.expenseplanner.R;
import com.zessta.expenseplanner.modal.Expense;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class ChartFragment extends Fragment {

    private Pie pie;
    private List<DataEntry> data = new ArrayList<DataEntry>(
            Arrays.asList(new ValueDataEntry("SUNDAY", 0),
                    new ValueDataEntry("MONDAY", 0),
                    new ValueDataEntry("TUESDAY", 0),
                    new ValueDataEntry("WEDNESDAY", 0),
                    new ValueDataEntry("THURSDAY", 0),
                    new ValueDataEntry("FRIDAY", 0),
                    new ValueDataEntry("SATURDAY", 0)));

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.chart_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AnyChartView anyChartView = view.findViewById(R.id.any_chart_view);
        anyChartView.setProgressBar(view.findViewById(R.id.progress_bar));
        pie = AnyChart.pie();

        pie.setOnClickListener(new ListenersInterface.OnClickListener(new String[]{"x", "value"}) {
            @Override
            public void onClick(Event event) {
//                Toast.makeText(ChartFragment.this, event.getData().get("x") + ":" + event.getData().get("value"), Toast.LENGTH_SHORT).show();
            }
        });


        pie.data(data);

        pie.title("Total Amount spent last week");

//        pie.labels().position("outside");
//
//        pie.legend().title().enabled(true);
//        pie.legend().title()
//                .text("Retail channels")
//                .padding(0d, 0d, 0d, 0d);
//
//        pie.legend()
//                .position("center-bottom")
//                .itemsLayout(LegendLayout.HORIZONTAL)
//                .align(Align.CENTER);

        anyChartView.setChart(pie);
    }

    public void updateData(ArrayList<Expense> weekExpenses) {
        HashMap<String, ValueDataEntry> data = new HashMap<>();
        double totalAmount = 0.0;
        for (int i = 0; i < weekExpenses.size(); i++) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(weekExpenses.get(i).date);
            totalAmount = totalAmount + weekExpenses.get(i).amount;
            switch (calendar.get(Calendar.DAY_OF_WEEK)) {
                case Calendar.SUNDAY: {
                    if (data.containsKey("SUNDAY")) {
                        Double amount = Double.valueOf((String) data.get("SUNDAY").getValue("value"));
                        amount = amount + weekExpenses.get(i).amount;
                        data.put("SUNDAY", new ValueDataEntry("SUNDAY", amount));
                    } else {
                        data.put("SUNDAY", new ValueDataEntry("SUNDAY", weekExpenses.get(i).amount));
                    }
                    break;
                }
                case Calendar.MONDAY: {
                    if (data.containsKey("MONDAY")) {
                        Double amount = Double.valueOf((String) data.get("MONDAY").getValue("value"));
                        amount = amount + weekExpenses.get(i).amount;
                        data.put("MONDAY", new ValueDataEntry("MONDAY", amount));
                    } else {
                        data.put("MONDAY", new ValueDataEntry("MONDAY", weekExpenses.get(i).amount));
                    }
                    break;
                }
                case Calendar.TUESDAY: {
                    if (data.containsKey("TUESDAY")) {
                        Double amount = Double.valueOf((String)data.get("TUESDAY").getValue("value"));
                        amount = amount + weekExpenses.get(i).amount;
                        data.put("TUESDAY", new ValueDataEntry("TUESDAY", amount));
                    } else {
                        data.put("TUESDAY", new ValueDataEntry("TUESDAY", weekExpenses.get(i).amount));
                    }
                    break;
                }
                case Calendar.WEDNESDAY: {
                    if (data.containsKey("WEDNESDAY")) {
                        Double amount = Double.valueOf((String) data.get("WEDNESDAY").getValue("value"));
                        amount = amount + weekExpenses.get(i).amount;
                        data.put("WEDNESDAY", new ValueDataEntry("WEDNESDAY", amount));
                    } else {
                        data.put("WEDNESDAY", new ValueDataEntry("WEDNESDAY", weekExpenses.get(i).amount));
                    }
                    break;
                }
                case Calendar.THURSDAY: {
                    if (data.containsKey("THURSDAY")) {
                        Double amount = Double.valueOf((String)data.get("THURSDAY").getValue("value"));
                        amount = amount + weekExpenses.get(i).amount;
                        data.put("THURSDAY", new ValueDataEntry("THURSDAY", amount));
                    } else {
                        data.put("THURSDAY", new ValueDataEntry("THURSDAY", weekExpenses.get(i).amount));
                    }
                    break;
                }
                case Calendar.FRIDAY: {
                    if (data.containsKey("FRIDAY")) {
                        Double amount = Double.valueOf((String)data.get("FRIDAY").getValue("value"));
                        amount = amount + weekExpenses.get(i).amount;
                        data.put("FRIDAY", new ValueDataEntry("FRIDAY", amount));
                    } else {
                        data.put("FRIDAY", new ValueDataEntry("FRIDAY", weekExpenses.get(i).amount));
                    }
                    break;
                }
                case Calendar.SATURDAY: {
                    if (data.containsKey("SATURDAY")) {
                        Double amount =  Double.valueOf((String)data.get("SATURDAY").getValue("value"));
                        amount = amount + weekExpenses.get(i).amount;
                        data.put("SATURDAY", new ValueDataEntry("SATURDAY", amount));
                    } else {
                        data.put("SATURDAY", new ValueDataEntry("SATURDAY", weekExpenses.get(i).amount));
                    }
                    break;
                }
            }

            ArrayList<DataEntry> arrayList = new ArrayList<>();
            arrayList.addAll(data.values());
            pie.data(arrayList);
            pie.title("Total Amount spent in last 7 days : ₹" + totalAmount);

        }
    }
}
