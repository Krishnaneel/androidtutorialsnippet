package com.zessta.expenseplanner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.zessta.expenseplanner.adapter.ExpenseListAdapter;
import com.zessta.expenseplanner.modal.Expense;
import com.zessta.expenseplanner.screen.ChartFragment;
import com.zessta.expenseplanner.screen.ExpenseModalSheet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    private ExpenseListAdapter adapter;
    private ChartFragment chartFragment;
    private Helper helper;

    ArrayList<Expense> expenses = new ArrayList<>(
//            Arrays.asList(
//            new Expense("1", "Shoes", 24.5, new Date()),
//            new Expense("1", "Watch", 120, new Date()),
//            new Expense("1", "Milk", 2.5, new Date()),
//            new Expense("1", "Bed", 50, new Date()),
//            new Expense("1", "Mobile", 60, new Date()))
    );

    private void addExpenseToList(Expense expense) {
        helper.addExpense(expense);
        expenses.add(expense);
        adapter.notifyDataSetChanged();
        updateChart();
    }

    private void updateChart() {
        ArrayList<Expense> chartExpenses = new ArrayList<>();
        for (int i = 0; i < expenses.size(); i++) {
            Calendar currentCalendar = Calendar.getInstance();
            currentCalendar.set(Calendar.DATE, -7);
            Calendar expenseCalendar = Calendar.getInstance();
            expenseCalendar.setTime(expenses.get(i).date);

            if (TimeUnit.MILLISECONDS.toDays(
                    currentCalendar.getTimeInMillis() - expenseCalendar.getTimeInMillis()) <= 7) {
                chartExpenses.add(expenses.get(i));
            }
        }

        chartFragment.updateData(chartExpenses);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        helper = new Helper(MainActivity.this);
        expenses = helper.getAllExpenses();
        RecyclerView expenseList = findViewById(R.id.expense_list_rv);
        expenseList.setLayoutManager(new LinearLayoutManager(this));
        expenseList.addItemDecoration(new DividerItemDecoration(expenseList.getContext(),
                DividerItemDecoration.VERTICAL));

        chartFragment = new ChartFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.chart_fl, chartFragment);
        fragmentTransaction.commit();

        adapter = new ExpenseListAdapter(expenses);
        expenseList.setAdapter(adapter);

        FloatingActionButton addExpenseFab = findViewById(R.id.add_expense_fab);
        addExpenseFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ExpenseModalSheet expenseModalSheet = new ExpenseModalSheet(new ExpenseModalSheet.callback() {

                    @Override
                    public void addExpense(Expense expense) {
                        addExpenseToList(expense);
                    }
                });
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.add(expenseModalSheet, expenseModalSheet.getTag());
                fragmentTransaction.commit();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateChart();
    }
}
