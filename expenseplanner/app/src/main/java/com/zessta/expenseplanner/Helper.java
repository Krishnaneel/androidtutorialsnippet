package com.zessta.expenseplanner;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.zessta.expenseplanner.modal.Expense;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Helper extends SQLiteOpenHelper {
    public static final String TABLE = "expenseTable";
    public static final int DATABSE_VERSION = 1;
    public static final String DATABASE = "expenseDatabase";

    //Expense Column Names;
    public static final String ID = "ID";
    public static final String TITLE = "title";
    public static final String AMOUNT = "amount";
    public static final String DATE = "date";

    public Helper(Context context) {
        super(context, DATABASE, null, DATABSE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String createTable = "CREATE TABLE IF NOT EXISTS " + TABLE + "("
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + TITLE + " TEXT,"
                + AMOUNT + " REAL,"
                + DATE + " REAL" + ")";
        sqLiteDatabase.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE);
        onCreate(sqLiteDatabase);
    }

    public void addExpense(Expense expense) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(expense.date);
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TITLE, expense.title);
        contentValues.put(AMOUNT, expense.amount);
        contentValues.put(DATE, calendar.getTimeInMillis());
        db.insert(TABLE, null, contentValues);
        db.close();
    }

    public Expense getExpense(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE, new String[]{ID, TITLE, AMOUNT, DATE}, ID + "=?"
                + new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        } else return null;
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTimeInMillis(cursor.getLong(3));
        Date date = calendar1.getTime();
        return new Expense(cursor.getString(0), cursor.getString(1), cursor.getFloat(2), date);
    }

    public ArrayList<Expense> getAllExpenses() {

        ArrayList<Expense> expenseList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTimeInMillis(cursor.getLong(3));
                Date date = calendar1.getTime();
                Expense student = new Expense(
                        cursor.getString(0),
                        cursor.getString(1),
                        cursor.getFloat(2),
                        date
                );

                expenseList.add(student);
            } while (cursor.moveToNext());
        }

        return expenseList;
    }
}
