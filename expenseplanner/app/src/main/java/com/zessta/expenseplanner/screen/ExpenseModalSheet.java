package com.zessta.expenseplanner.screen;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.zessta.expenseplanner.MainActivity;
import com.zessta.expenseplanner.R;
import com.zessta.expenseplanner.modal.Expense;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;

public class ExpenseModalSheet extends BottomSheetDialogFragment {
    callback callback;
    private MainActivity mainActivity;

    public ExpenseModalSheet() {
        // doesn't do anything special
    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity) context;
    }

    public ExpenseModalSheet(callback callback) {
        this.callback = callback;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.expense_modal_sheet, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        final EditText title = view.findViewById(R.id.title);
        final EditText amount = view.findViewById(R.id.amount);
        final TextView dateTv = view.findViewById(R.id.date_tv);
        final Calendar selectedCalendar = Calendar.getInstance();

        MaterialDatePicker.Builder builder = MaterialDatePicker.Builder.datePicker();
        CalendarConstraints.Builder constraints = new CalendarConstraints.Builder();
        constraints.setEnd(Calendar.getInstance().getTimeInMillis());
        builder.setCalendarConstraints(constraints.build());


        final MaterialDatePicker picker = builder.build();
        picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener() {
            @Override
            public void onPositiveButtonClick(Object selection) {
                selectedCalendar.setTimeInMillis((Long) selection);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
                dateTv.setText(simpleDateFormat.format(selectedCalendar.getTime()));
            }
        });

        final AppCompatButton pickDatePicker = view.findViewById(R.id.pick_date_btn);

        pickDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                picker.show(mainActivity.getSupportFragmentManager(),"");
            }
        });

        view.findViewById(R.id.add_expense_modal_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.addExpense(new Expense("4", title.getText().toString(), Double.valueOf(amount.getText().toString()), selectedCalendar.getTime()));
                dismiss();
            }
        });
    }

    public interface callback {
        void addExpense(Expense expense);
    }
}
