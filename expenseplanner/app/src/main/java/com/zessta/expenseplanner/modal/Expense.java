package com.zessta.expenseplanner.modal;

import java.util.Date;

public class Expense {

    public final String id;
    public final String title;
    public final double amount;
    public final Date date;

    public Expense(String id, String title, double amount, Date date){
        this.id = id;
        this.title = title;
        this.amount = amount;
        this.date = date;
    }
}
